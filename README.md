# ROR API Matching Script

## Description
This R script takes as input a list of affiliation strings, and queries the ROR API using the [affiliation parameter](https://ror.readme.io/docs/affiliation-parameter) to return a set of likely candidate RORIDs for manual checking.
