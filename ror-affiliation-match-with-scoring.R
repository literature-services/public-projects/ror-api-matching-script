library(tidyverse)
library(httr)
library(progress)

# Functions ----

# Function to query ROR with an affiliation string
# Returns all matched results with scoring metrics

ror_matches <- function(affiliation){
  url <- str_c('https://api.ror.org/organizations?affiliation=',
               URLencode(affiliation, reserved = T))
  resp <- httr::GET(url) %>% 
    content(encoding = 'UTF-8')
  tbl <- resp$items %>% {
    tibble(score = map_dbl(., "score"),
                matching_type = map_chr(., "matching_type"),
                chosen = map_lgl(., "chosen"),
                organization = map(., "organization"))
    } %>% 
    mutate(id = str_replace(map_chr(organization, "id"), '^https://ror.org/', ''),
           ror_name = map_chr(organization, "name"),
           country = map(organization, "country"),
           country_code = map_chr(country, "country_code")) %>% 
    select(-c(organization, country))
  Sys.sleep(0.1)
  return(tbl)
}

# Map function with added progress bar

map_progress <- function(.x, .f, ...) {
  .f <- purrr::as_mapper(.f, ...)
  pb <- progress::progress_bar$new(total = length(.x), 
                                   format = " [:bar] :current/:total (:percent) eta: :eta", force = TRUE)
  f <- function(...) {
    pb$tick()
    .f(...)
  }
  purrr::map(.x, f, ...)
}

# Import affiliation strings ----
# Import affiliations as a dataframe; reading a CSV is given as an example
# Single column name should be "affiliation"

affiliations <- read_csv(file.choose())

# Query ROR API ----
# The "score" and "chosen" filters can be adjusted for precision vs. recall
# The current parameters lean towards precision
# Try ~0.8 and remove the "chosen" filter for more potential matches ...
# ... at the expense of more manual checking

matches <- affiliations %>% 
  mutate(ror = map_progress(affiliation, ror_matches)) %>% 
  unnest(ror) %>% 
  filter(score > 0.95) %>%
  filter(chosen == TRUE)

# Export to a CSV for manual checking ----

matches %>% relocate(ror_name, .after = affiliation) %>% 
  write_csv(., str_c('./ror-api-export-', Sys.Date(), '.csv'), quote = 'all')
